<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.hampelgroup.com/
 * @since             1.0.0
 * @package           Postindex
 *
 * @wordpress-plugin
 * Plugin Name:       Postindex
 * Plugin URI:        https://bitbucket.org/hampel/postindex
 * Description:       Adds [postindex] shortcode
 * Version:           1.0.0
 * Author:            Simon Hampel
 * Author URI:        http://www.hampelgroup.com/
 * License:           MIT
 * License URI:       https://opensource.org/licenses/MIT
 * Text Domain:       postnav
  */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function postindex_shortcode( $atts )
{
    $atts = shortcode_atts([
        'id' => 0,
    ], $atts );

	$id = $atts['id'];
	$replace = '';

	if ($id > 0) {
		$postref = get_post($id);
		if (!empty($postref)) {
			$permalink = get_permalink($id);
			$replace = '<a href="' . $permalink . '">' . $postref->post_title . '</a>';
			$replace = '<div class="postindex">Index&nbsp;&raquo;&nbsp;' . $replace . '</div>';
		}
	}

	return $replace;
}
add_shortcode( 'postindex', 'postindex_shortcode' );
